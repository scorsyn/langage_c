#include <stdio.h>
#include <stdlib.h>
#include <math.h>



int parfait01(int n)
{
	int i,somme=0;
	for (i=1; i<n; i++)
	{
		if (n%i==0)
			somme=somme+i;
		if (somme>n)
			return 0;
	}
	if (somme==n)
		printf("%d est un nombre parfait",n);
	return 0;
}

int parfait02(int n)
{
	int i,somme=0;
	if (n%2==0)
	{
		for (i=1; i<n; i++)
		{
			if (n%i==0)
				somme=somme+i;
			if (somme>n)
				return 0;
		}
		printf("Somme des diviseurs : %d\n",somme);
		if (somme==n)
			printf("%d est un nombre parfait",n);
		return 0;
	}
	else
	{
		for (i=1; i<n; i=i+2)
		{
			if (n%i==0)
				somme=somme+i;
			if (somme>n)
				return 0;
		}
		printf("Somme des diviseurs : %d\n",somme);
		if (somme==n)
			printf("%d est un nombre parfait",n);
		return 0;
	}
}


int parfait03(int n)
{
	int i,somme=1;
	if (n%2==0)
	{
		for (i=2; i<=sqrt(n); i++)
		{
			if (n%i==0)
			{
				if (i==(n/i))
					somme=somme+i;
				else
					somme=(somme+i+(n/i));
				//      if (somme>n)
				//        return 0;
			}
		}
		printf("Somme des diviseurs : %d\n",somme);
		if (somme==n)
			printf("%d est un nombre parfait",n);
		return 0;
	}
	else
	{
		for (i=3; i<=sqrt(n); i=i+2)
		{
			if (n%i==0)
			{
				if (i==(n/i))
					somme=somme+i;
				else
					somme=(somme+i+(n/i));
				//        if (somme>n)
				//          return 0;
			}
		}
		printf("Somme des diviseurs : %d\n",somme);
		if (somme==n)
			printf("%d est un nombre parfait",n);
		return 0;
	}
}

int parfait1(int n)
{
	int i,somme=0;
	for (i=1; i<n; i++)
	{
		if (n%i==0)
			somme=somme+i;
		if (somme>n)
			return 0;
	}
	if (somme==n)
		return 1;
	return 0;
}

int parfait2(int n)
{
	int i,somme=0;
	if (n%2==0)
	{
		for (i=1; i<n; i++)
		{
			if (n%i==0)
			{
				somme=somme+i;
				if (somme>n)
					return 0;
			}
		}
		// printf("Somme des diviseurs : %d\n",somme);
		if (somme==n)
			return 1;
		return 0;
	}
	else
	{
		for (i=1; i<n; i=i+2)
		{
			if (n%i==0)
			{
				somme=somme+i;
				if (somme>n)
					return 0;
			}
		}
		// printf("Somme des diviseurs : %d\n",somme);
		if (somme==n)
			return 1;
		return 0;
	}
}


int parfait3(int n)
{
	int i,somme=1;
	if (n%2==0)
	{
		for (i=2; i<=sqrt(n); i++)
		{
			if (n%i==0)
			{
				if (i==(n/i))
				{
					somme=somme+i;
				}
				else
				{
					somme=(somme+i+(n/i));
				}
				if (somme>n)
					return 0;
			}
		}
		if (somme==n)
			return 1;
		return 0;
	}
	else
	{
		for (i=3; i<=sqrt(n); i=i+2)
		{
			if (n%i==0)
			{
				if (i==(n/i))
					somme=somme+i;
				else
					somme=(somme+i+(n/i));
				if (somme>n)
					return 0;
			}
		}
		if (somme==n)
			return 1;
		return 0;
	}
}


int parfait3opti(int n)
{
	int i,rac,somme=1;
	rac=(int)sqrt(n);
	if (n==rac*rac)
		somme=somme-sqrt(n);
	if (n%2==0)
	{
		for (i=2; i<=sqrt(n); i++)
		{
			if (n%i==0)
			{
				somme=(somme+i+(n/i));
				if (somme>n)
					return 2;
			}
		}
		if (somme==n)
			return 1;
		return 0;
	}
	else
	{
		for (i=3; i<=sqrt(n); i=i+2)
		{
			if (n%i==0)
			{
				somme=(somme+i+(n/i));
				if (somme>n)
					return 2;
			}
		}
		if (somme==n)
			return 1;
		return 0;
	}
}


int parfait4opti(int n)
{
	int i,rac,somme=1;
	rac=(int)sqrt(n);
	if (n==rac*rac)
		somme=somme-sqrt(n);
	if (n%2==0)
	{
		for (i=2; i<=sqrt(n); i++)
		{
			if (n%i==0)
			{
				somme=(somme+i+(n/i));
			}
		}
		if (somme==n)
			return 1;
		return 0;
	}
	else
	{
		for (i=3; i<=sqrt(n); i=i+2)
		{
			if (n%i==0)
			{
				somme=(somme+i+(n/i));
			}
		}
		if (somme==n)
			return 1;
		return 0;
	}
}


void parfaitQC1(int n,int * res)
{	int i,rac,somme=1;
	rac=(int)sqrt(n);
	if (n==rac*rac)
		somme=somme-sqrt(n);
	if (n%2==0)
	{	for (i=2; i<=sqrt(n); i++)
		{   if (n%i==0)
			{
				somme=(somme+i+(n/i));
				if (somme>n) *res=0;
			}
		}
		if (somme==n)
			{*res=1;
			return 0;
			}
		*res=0;
		return 0;
	}
	else
	{	for (i=3; i<=sqrt(n); i=i+2)
		{   if (n%i==0)
			{	somme=(somme+i+(n/i));
				if (somme>n)
					{*res=0;
					return 0;
					}
			}
		}
		if (somme==n)
			{*res=1;
			return 0;
			}
		*res=0;
		return 0;
	}
}


void parfait_QC2(int n,int *res)
{
*res=parfait3opti(n);
}


void carre(int n)
{
	int rac;
	rac=floor(sqrt(n));
	if (n==rac*rac)
		printf("est carre");
}

void affichetous (int n)
{
	int i,k;
	k=0;
	for (i=2; i<=n; i++)
	{
		if (parfait3(i)==1)
		{
			printf("%d \n",i);
			k++;
		}
		if (k==25)
		{
			system("pause");
			k=0;
		}
	}
}



void affichenn (int naff)
{
	int i,k;
	k=1;
	i=2;
	while (k<=naff)
	{
		if (parfait4opti(i)==1)
		{
			printf("%d \n",i);
			if (k%25==0)
			{
				system("pause");
			}
			k=k+1;

		}
		i++;
	}

}



int sommesuppn(int n)
{
	int i,k=0,somme=0;
	for (i=1; i<n/2; i++)
	{
		if (n%i==0)
		{
			somme=somme+i;
			//       printf("%d %d \n",somme,n);
			if (somme>n)
				//k=k+1;
				//printf("%d ",n);
				return 1;
		}
	}
	// printf("Somme des diviseurs : %d\n",somme);
	if (somme==n)
		//  printf("parfait \n");
		return 0;
	return 0;
}

int abondant(int n,int *k)
{	int i,rac,somme=1;
	rac=(int)sqrt(n);
	if (n==rac*rac)
		somme=somme-sqrt(n);
	if (n%2==0)
	{	for (i=2; i<=sqrt(n); i++)
		{	if (n%i==0)	somme=(somme+i+(n/i));
		}
		if (somme>n) return 1;
		return 0;
	}
	else
	{	for (i=3; i<=sqrt(n); i=i+2)
		{	if (n%i==0) somme=(somme+i+(n/i));
		}
		if (somme>n) return 1;
		return 0;
	}
}

int main()
{
	int i,n,k,compteur=0,parfait;
	printf("Saisir un nombre :\n");
	scanf("%d",&n);
	//parfait2(n);
	//printf("%d \n",parfait1(n));
	//printf("%d \n",parfait2(n));
	//printf("%d \n",parfait3(n));
	//affichetous(n);
	affichenn(n);
	//parfaitQC1(n,&parfait);
	//printf("%d",parfait);
    //parfaitQC1(n,&parfait);
	//printf("%d",parfait);
	// printf("%d \n",parfait4(n,0));
	//carre(n);
	// for (i=1;i<=100000;i=i+1)
//  {sommesuppn(i);
	//k=k+sommesuppn(i);}
//  printf("%d \n",k);
	//  printf("%d \n",sommesuppn(28));
	/* k=0;
	 for (i=1;i<=10000000;i++)
	 {abondant(i,&k);
	 compteur=abondant(i,&k)+compteur;
	  }
	  printf("%d \n",compteur);*/
  // printf("%d \n",parfait3opti(n));

   // printf("%d \n",parfait4opti(n));

	return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char t[100];  //t statique ou dynamique - il est conseillé de présenter les diverses versions vues dans le TD2

void saisie_a(char * t)
{   char c;
    do
    {   c=getchar();
        *t=c;
        t++;
    }
    while (c!='\n');
    t--;
    *t='\0';
}

void copie(char s1[],char s2[])
{int i;
for(i=0;s1[i]!='\0';i++)
    {s2[i]=s1[i];
    }
}

//char tmp[1000]; et dans ce cas pas de déclaration dynamique de tmp

char * saisie_b()
{   char * tmp,*res; int taille;
    fflush(stdin);  //sert à vider le buffer de saisie dans le cas où il contiendrait '\n'
    tmp=(char *)malloc(1000*sizeof(char));
    saisie_a(tmp);
    taille=strlen(tmp)+1;
    res=(char *)malloc(taille*sizeof(char));
    copie(tmp,res);
    free(tmp);
    return res;
}

/*
char * saisie_b()
{
    char * tmp,*p1,*p2,*res; int taille; //char tmp[1000]; et dans ce cas pas de déclaration dynamique de tmp
    fflush(stdin);
    p1=tmp=(char *)malloc(1000*sizeof(char));
    saisie_a(tmp);
    taille=strlen(tmp)+1;
    p2=res=(char *)malloc(taille*sizeof(char));
    while((*p2++=*p1++)!='\0');
    free(tmp);
    return res;
}
*/

char * saisie_c()
{   char *nouv,*anc,c,*p1,*p2; int nb,i;
    nb=1;
    i=0;
    anc=nouv=p1=p2=NULL;
    do
        {c=getchar();
        p1=nouv=(char*)malloc(nb*sizeof(char));
        for(i=0;i<nb-1;i++) nouv[i]=anc[i];//ou i<=nb-2 //pour la première itération, on ne rentre pas dans la boucle
        nouv[i]=c;
        if (anc!=NULL) free(anc);
        anc=nouv;
        nb++;
        }
    while(c!='\n');
    nouv[i]='\0';
    return nouv;
}

char * saisie_c_2()
{   char *nouv,*anc,c,*p1,*p2; int nb,i;
    nb=1;
    i=0;
    anc=nouv=p1=p2=NULL;
    do
    {   c=getchar();
        p1=nouv=(char*)malloc(nb*sizeof(char));
        for(i=0,p2=anc;i<nb-1;i++) *p1++=*p2++;//ou i<=nb-2 //pour la première itération, on ne rentre pas dans la boucle
        *p1=c;
        if (anc!=NULL) free(anc);
        anc=nouv;
        nb++;
    }
    while(c!='\n');
    *p1='\0';
    return nouv;
}

void affichage(char * t)
{int i;
if (t!=NULL) for (i=0;t[i]!='\0';i++) printf("%2c",t[i]);
    printf("\n");
}

void affichage_2(char * t)
{
if (t!=NULL)
    {while(*t!='\0')
        {
        printf("%2c",*t);
        t++;
        }
        printf("\n");
    }
}

void occurrence(char *tab,char car,int *p_taille,int *p_occ)
{int taille=0;
int occ=0;
while (*tab!='\0')
    {taille++;
    if (*tab++==car) occ++;
    }
    *p_taille=taille;
    *p_occ=occ;
}

void suppression(char ** psource,char car)
{int i,taille,occ;
char *cible,*p1,*p2;
    p1=*psource;
    occurrence(*psource,car,&taille,&occ);
    if (taille!=occ)  //on gère le cas où la cible ne contient rien
    {
        p2=cible=(char*)malloc((taille-occ+1)*sizeof(char));
    for (i=0;i<=taille;i++)
        {
        if (*p1!=car) *p2++=*p1;
        p1++;
        }
    }
    if (taille==occ) cible=NULL;
    free(*psource);
    *psource=cible;
}


void dedoublement(char ** psource,char car)
{
int i,taille,occ;
char *cible,*p1,*p2;
    p1=*psource;
    occurrence(*psource,car,&taille,&occ);
    p2=cible=(char*)malloc((taille+occ+1)*sizeof(char));
    for (i=0;i<=taille;i++)
        {
        *p2++=*p1;
        if (*p1==car) *p2++=car;
        p1++;
        }
    free(*psource);
    *psource=cible;
}

void doublement(char ** psource)
{
int i,taille;
char *cible=NULL;
char *p1,*p2;
    p1=*psource;
    taille=strlen(p1);
    p2=cible=(char*)malloc((taille*2+1)*sizeof(char));
    for (i=0;i<=taille;i++)
        {
        *p2++=*p1;
        *p2++=*p1++;
        }
    free(*psource);
    *psource=cible;
}

int * comptage(char *tabl)
{char *p1,car;
int  *t_occ;
t_occ=(int*)malloc(28*sizeof(int));
int i;
p1=tabl;
for (i=0;i<=27;i++) t_occ[i]=0;
while (*p1!='\0')
    {car=tolower(*p1);
    if ((car>='a') && (car<='z')) t_occ[car-'a']++;
    else t_occ[26]++;
    p1++;
    t_occ[27]++;
    }
    return t_occ;
}


int * comptagebis(char *tabl, int *nbcs,int *nbtc)
{
int i;
int *p1,*p2;
int  *tab_occ;
p1=tab_occ=(int*)malloc(26*sizeof(int));
p2=comptage(tabl);
for (i=0;i<=25;i++) *p1++=*p2++;
*nbcs=*p2++;
*nbtc=*p2;
//free(p2);
return tab_occ;
}

void affichage_occ2(int *tab_occ)
{
int  *p,i;
p=tab_occ;
for (i=0;i<26;i++)
    {
    if (*p>0) printf("il y a %d caractères %c \n",*p,i+'a');
    p++;
    }
    printf("il y a %d caractères spéciaux \n",*p++);
    printf("il y a %d caractères \n",*p);
}

void affichage_occ(char *tabl)
{
int  *p,*tab_occ,cs,tc,i;
p=tab_occ=comptagebis(tabl,&cs,&tc);
for (i=0;i<26;i++)
    {
    if (*p>0) printf("il y a %d caractères %c \n",*p,i+'a');
    p++;
    }
    printf("il y a %d caractères spéciaux \n",cs);
    printf("il y a %d caractères \n",tc);
}

char ** saisieensch1(int nb)
{int i;
//char ** tch;
char ** tch,**p;
tch=(char**)malloc(nb*sizeof(char*));
for (p=tch;p<tch+nb;p++)
//for (i=0;i<nb;i++)
{printf("entrer une chaine \n");
//tch[i]=saisie_c();
*p=saisie_b();
}
return tch;
}

void afficheensch1(char ** tch, int nb)
{int i;
for (i=0;i<nb;i++)
affichage(tch[i]);
//printf("%s \n",tch[i]);
}

char ** saisieensch(int nb)
{char ** tch,**p;
tch=(char**)malloc((nb+1)*sizeof(char*));
for (p=tch;p<tch+nb;p++)
{printf("entrer une chaine \n");
*p=saisie_c();
}
*p=NULL;
return tch;
}

char ** saisieensch_2(int nb)
{char ** tch;
int i;
tch=(char**)malloc((nb+1)*sizeof(char*));
for (i=0;i<nb;i++)
    {printf("entrer une chaine \n");
    tch[i]=saisie_c();
    }
tch[i]=NULL;
return tch;
}

void afficheensch(char ** tch)
{
int i;
if (tch!=NULL)
    {
    for (i=0;tch[i]!=NULL;i++) affichage(tch[i]);
    }
}


void afficheenschbis(char ** tch)
{
char **p1,*p2;
if (tch!=NULL)
    {
    for (p1=tch;*p1!=NULL;p1++)
        {for(p2=*p1;*p2!='\0';p2++) printf("%c",*p2);
        printf("\n");
        }
    }
}

void afficheenschbis2(char ** tch)
{
char **p1;
if (tch!=NULL)
    {
    for (p1=tch;*p1!=NULL;p1++) affichage(*p1);
    printf("\n");
    }
}

     //faire version avec tableau dynamique type saisie_c

char **saisieenschbis()
{char **tch;
char *tmp[1000];
int i,j,choix;
i=0;
do
    {printf("Voulez-vous saisir une chaine ? (0/1)");
    scanf("%d",&choix);
    getchar();
    if (choix==1)
        {printf("saisir une chaine \n");
        tmp[i++]=saisie_b();
        }
    }while (choix!='\0');
    if (i==0) return NULL;
    tch=(char**)malloc((i+1)*sizeof(char*));
    for(j=0;j<i;j++) tch[j]=tmp[j];
    tch[j]=NULL;
    return tch;
}

void dedoubleensch (char ** tch,char car)
{
char **p;
for (p=tch;*p!=NULL;p++)
dedoublement(p,car);
}

void dedoubleensch_2 (char ** tch,char car)
{
for (;*tch!=NULL;tch++) dedoublement(tch,car);
}

void dedoubleensch_3 (char ** tch,char car)
{
int i;
for (i=0;tch[i]!=NULL;i++)
dedoublement(&tch[i],car);
}

void dedoubleensch_4 (char ** tch,char car)
{
int i;
for (i=0;tch[i]!=NULL;i++)
dedoublement(&*(tch+i),car);
}

//appel tch=suppensch(&tch,'a');

void suppensch(char *** tch, char car)
{int i,j,taille,occ;
char **res,*q;
j=0;
for (i=0,j=0;(*tch)[i]!=NULL;i++)
    {occurrence((*tch)[i],car,&taille,&occ);
    if (taille!=occ) j++;
    }
if (j!=0)
    {
    res=(char**)malloc((j+1)*sizeof(char*));
    for (i=0,j=0;(*tch)[i]!=NULL;i++)
        {q=(*tch)[i];
        suppression(&q,car);
        if (q!=NULL) res[j++]=q;
        }
    res[j]=NULL;
    *tch=res;
    }
else *tch=NULL;
}

void supprimeenschbis (char *** tch,char car)
{   char **tmp,**res,*p;
    int i,j;
    for (i=0;(*tch)[i]!=NULL;i++);
    tmp=(char**)malloc((i+1)*sizeof(char*));
    j=0;
    for (i=0,j=0;(*tch)[i]!=NULL;i++)
        {p=(*tch)[i];
        suppression(&p,car);
        if (p!=NULL)
            {tmp[j++]=p;
            }
        }
    if (j!=0)
        {
        tmp[j]=NULL;
        res=(char**)malloc((j+1)*sizeof(char*));
        for (i=0;tmp[i]!=NULL;i++) res[i]=tmp[i];
        res[i]=NULL;
        free(tmp);
        *tch=res;
        }
    else *tch=NULL;
}

void supprimechainecar (char * ** tch,char car)
{   char **res;
    int cpt,i,j;
    for (cpt=0,i=0;(*tch)[i]!=NULL;i++) if ((*tch)[i][0]==car) cpt++;
    if (cpt!=0)
    {
    res=(char**)malloc((i+1-cpt)*sizeof(char*));
    for (i=0,j=0;(*tch)[i]!=NULL;i++)
        {
        if ((*tch)[i][0]!=car) res[j++]=(*tch)[i];
        else free((*tch)[i]);
        }
    res[j]=NULL;
    *tch=res;
    }
    else *tch=NULL;
}

/*void supprimeensch (char ** * tch,char car)
{
int i;
char **p;
for (p=tch;*p!=NULL;p++)
//for (;*tch!=NULL;tch++)   //possible car tch passé par valeur
//for (i=0;tch[i]!=NULL;i++)

//suppression(&tch[i],car);
//suppression(&*(tch+i,car);
suppression(&p,car);
}*/

int main()
{   int occ,taille,nbl,i,cs,tc;
    char * ch1,carac,*ch,*chb,*chc;
    int  *tab_occ;
    tab_occ=(int*)malloc(28*sizeof(int));
    int  *tab_occ26;
    tab_occ26=(int*)malloc(26*sizeof(int));
/*
    printf("Saisie_a : Entrer une chaine :");
    saisie_a(t);
    affichage(t);

    printf("Saisie_b : Entrer une chaine :");
    chb=saisie_b();
    affichage(chb);

    printf("Saisie_c : Entrer une chaine :");
    chc=saisie_c();
    affichage(chc);

    printf("Dédoublement d'un caractère : Entrer une chaine :");
    ch1=saisie_c();
    printf("Entrer un caractère à dédoubler :");
    carac=getchar();
    dedoublement(&ch1,carac);
    affichage(ch1);

getchar();

    printf("Dédoublement de tous les caractères : Entrer une chaine :");
    ch1=saisie_c();
    doublement(&ch1);
    affichage(ch1);

    printf("Suppression d'un caractère : Entrer une chaine :");
    ch1=saisie_c();
    printf("Entrer un caractère à supprimer :");
    carac=getchar();
    suppression(&ch1,carac);
    affichage(ch1);

getchar();

    printf("Occurrence des caractères : Entrer une chaine :");
    ch1=saisie_c();
    tab_occ=comptage(ch1);
    affichage_occ2(tab_occ);
    free(tab_occ);

    printf("Occurrence des caractères : Entrer une chaine :");
    ch1=saisie_c();
    affichage_occ(ch1);
*/

    //free(tab_occ26);*/
//    char **tch;


ch1=saisieenschbis();
//afficheensch1(ch1,3);
//tch=saisieenschbis();
//afficheensch(tch);
//dedoubleensch(tch,'e');
//afficheensch(tch);
dedoubleensch(ch1,'a');
dedoubleensch_2(ch1,'b');
dedoubleensch_3(ch1,'c');
dedoubleensch_4(ch1,'d');
//afficheensch(ch1);
//afficheensch1(ch1,3);
//suppensch(&ch1,'a');
//supprimechainecar(&ch1,'b');
//supprimeenschbis(&ch1,'p');
//afficheensch(ch1);
afficheenschbis(ch1);
afficheenschbis2(ch1);
//saisie_a(tab);
//printf("%s",tab);

    return 0;
}


/*
void * comptagebis(char *tabl,int **tab_occ)
{
char *p1;
int  *t_occ;
t_occ=(int*)malloc(28*sizeof(int));
int i;
p1=tabl;
for (i=0;i<28;i++) printf("%d /",t_occ[i]);
for (i=0;i<=27;i++) t_occ[i]=0;
for (i=0;i<28;i++) printf("%d /",t_occ[i]);
i=0;
while (*p1!='\0')
//for (i=0;tabl[i]!='\0';i++)
    {
    i++;
    printf("test : %d \n",((*p1>='a') && (*p1<='z')));
//        printf("count : %d \n",tabl_occ[*p1-'a']);
            printf("ascii-a : %d \n",*p1-'a');
    if ((*p1>='a') && (*p1<='z')) t_occ[*p1-'a']++;
 //   printf("count : %d \n",tabl_occ[*p1-'a']);
    if ((*p1>='A') && (*p1<='Z')) t_occ[*p1-'A']++;
    if ((( *p1<'A') || (*p1>'Z')) && ((*p1<'a') || (*p1>'z'))) t_occ[26]++;
    p1++;
    t_occ[27]++;
//    for (i=0;i<28;i++) printf("%d /",t_occ[i]);
    }
       //for (i=0;i<28;i++) tab_occ[i]=t_occ[i];
       tab_occ=t_occ;
  //  for (i=0;i<28;i++) printf("%d /",tab_occ[i]);
}

void suppensch2(char ** * tch, char car)
{int i,j,taille,occ;
char ** tmp;
char **p,**q;
j=0;
p=*tch;
while(*p!=NULL)
    {
    occurrence(*p,car,&taille,&occ);
    if (taille!=occ) j++;
    p++;
    }
if (j==0) {
*tch=NULL;
return;}
q=tmp=(char**)calloc((j+1),sizeof(char*));
for (p=*tch;*p!=NULL;p++)
    {
    suppression(p,car);
    if (*p!=NULL) *q++=*p;
    //q++;
    }
    *q=NULL;
    *tch=tmp;
    free(tmp);
}
*/


/*
void suppensch(char *** tch, char car)
{int i,j,taille,occ;
char *tmp,*p,*q;
j=0;
for (p=*tch;*p!=NULL;p++)
    {occurrence(p,car,&taille,&occ);
    if (taille!=occ) j++;
    }
    printf("%d \n",j);
if (j!=0)
{
printf("toto");
q=tmp=(char*)malloc((j+1)*sizeof(char*));
for (p=*tch;*p!=NULL;p++)
    {occurrence(p,car,&taille,&occ);
    if (taille!=occ)
        {suppression(p,car);
        q=p;
        q++;
        }
    }
    q=NULL;
    *tch=tmp;
    //free(tmp);
}
else *tch=NULL;
}*/


    //printf("nb A:%d nb B:%d nb cs:%d \n",tab_occ[0],tab_occ[1],tab_occ[26]);
    //for (i=0;i<28;i++) printf("%d /",tab_occ[i]);
    //printf("\n");
     //for (i=0;i<26;i++) printf("%d /",tab_occ26[i]);
    //printf("   %d %d",cs,tc);
    //printf("\n");



    /*for (i=0;i<=10;i++)
{for (cs=0;cs<=10;cs++) printf("%4d",i+cs);
printf("\n");
}*/
 /*salut bande de cornichons*/
